//
//  Macros.h
//  Comasco
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#ifndef Macros_h
#define Macros_h
#define RGBColor(rgbValue, alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define HeightWithPercent(heightPercent) (heightPercent*[[UIScreen mainScreen] bounds].size.height)/100
#define WidthWithPercent(widthPercent) (widthPercent*[[UIScreen mainScreen] bounds].size.width)/100
#define HeightWithPercentWithObjectHeight(heightPercent, objectHeight) heightPercent*objectHeight/100
#define WidthWithPercentWithObjectWidth(widthPercent, objectWidth) widthPercent*objectWidth/100
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == 0)?TRUE:FALSE

#endif /* Macros_h */


