//
//  CurrentUser.h
//  stockApp
//
//  Created by Данияр Габбасов on 10.06.16.
//  Copyright © 2016 northalliance. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentUser : NSObject

+ (instancetype)instance;

+ (NSString *)getTagString :(NSString *)string;
+ (NSString *)getValueByTag:(NSString *)tag data:(NSArray<FormRow> *)data value:(NSString *)value;

@property (nonatomic, strong) FormModel *form;

@end
