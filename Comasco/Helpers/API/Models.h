//
//  Models.h
//  Comasco
//
//  Created by Vlad on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>

#import "API.h"

extern NSString * const FORM_METHOD;
extern NSString * const FORM_METHOD_ORDER;
extern NSString * const FORM_MODEL_SUCCESS;
extern NSString * const FORM_MODEL_FAIL;

#pragma mark - NSValueTransformer

@interface FormRowValueTrasformer : NSValueTransformer
@end

#pragma mark - Model

@interface Model : JSONModel
@property (nonatomic, assign) int code;
@end

@interface FormRow : JSONModel

@property (nonatomic, copy) NSString<Optional> *id;
@property (nonatomic, copy) NSString<Optional> *p_value_id;
@property (nonatomic, copy) NSString<Optional> *type;
@property (nonatomic, copy) NSString<Optional> *label;
@property (nonatomic, copy) NSString<Optional> *hint;
@property (nonatomic, copy) NSString<Optional> *value;
@property (nonatomic, copy) NSString<Optional> *value_text;
@property (nonatomic, copy) NSNumber<Optional> *order;
@property (nonatomic, copy) NSString<Optional> *cid;
@property (nonatomic, copy) NSNumber<Optional> *has_children;
@property (nonatomic, strong) NSArray<Optional> *values;
@property (nonatomic, strong) NSArray<Optional> *children;


//not model property
@property (nonatomic, copy) NSMutableArray<Optional> *currentValue;

-(XLFormRowDescriptor *)getRowByCondition:(NSString *)condition;

@end

@protocol FormRow
@end

@interface FormModel : Model
@property (nonatomic, strong) NSArray<FormRow> *data;
- (void)getForm;
- (NSArray*)getFormRowsByCondition :(NSString *)condition data:(NSArray<FormRow> *)data;
@end

@interface FormRowOutputModel : JSONModel
@property (nonatomic, strong) NSString<Optional> *label;
@property (nonatomic, strong) NSString<Optional> *controlId;
@property (nonatomic, strong) NSString<Optional> *controlType;
@property (nonatomic, strong) NSString<Optional> *value;
@property (nonatomic, strong) NSArray<Optional> *children;

- (void)setData :(FormRow *)data;

@end

@protocol FormRowOutputModel
@end

@interface FormInputModel : JSONModel
@property (nonatomic, assign) int orderId;
@end

@interface FormOutputModel : JSONModel
@property (nonatomic, strong) NSString *signature;
@property (nonatomic, strong) NSMutableArray<FormRowOutputModel> *orderData;

- (NSMutableArray<FormRowOutputModel> *)setFormData :(NSArray<FormRow> *)data formData:(NSDictionary *)formData;
- (void)request :(void(^)(FormInputModel *inputModel))success failure:(void(^)(NSString *errorMessage))failure;
@end
