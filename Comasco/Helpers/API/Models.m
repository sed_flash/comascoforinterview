//
//  Models.m
//  Comasco
//
//  Created by Vlad on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "Models.h"

NSString * const FORM_METHOD = @"ordering/form";
NSString * const FORM_METHOD_ORDER = @"ordering/order";
NSString * const FORM_MODEL_SUCCESS = @"FORM_MODEL_SUCCESS";
NSString * const FORM_MODEL_FAIL = @"FORM_MODEL_FAIL";

#pragma mark - NSValueTransformer

@implementation FormRowValueTrasformer

+ (Class)transformedValueClass {
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation {
    return NO;
}

- (id)transformedValue:(id)value {
    if (!value) return nil;
    if ([value isKindOfClass:[FormRow class]]) {
        FormRow *val = (FormRow *)value;
        
        NSMutableArray *displayValues = [NSMutableArray new];
        for (NSString *tag in val.currentValue) {
            [displayValues addObject:[CurrentUser getValueByTag:tag data:nil value:@""]];
        }
        
        NSLog(@"display value: %@", displayValues);
        
        return [displayValues componentsJoinedByString:@", "];
    }
    if ([value isKindOfClass:[NSString class]]) {
        return [NSString stringWithFormat:@"%@", value];
    }
    return nil;
}

@end

#pragma mark - Model

@implementation Model
@end

@implementation FormRow {
}

//- (NSArray<Optional> *)values {
//    NSMutableArray *valuesArray = [NSMutableArray new];
//    
//    if ([_values isKindOfClass:[NSDictionary class]]) {
//        for (NSDictionary *currentValue in _values) {
//            FormRow *currentFormRow = [[FormRow alloc] initWithDictionary:currentValue error:nil];
//            [valuesArray addObject:currentFormRow];
//        }
//    }
//    
//    return valuesArray;
//}

-(XLFormRowDescriptor *)getRowByCondition:(NSString *)condition {
    XLFormRowDescriptor *row;
    NSString *newString = [CurrentUser getTagString:self.id];
    
    if ([self.type isEqualToString:@"textbox"]) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:newString rowType:XLFormRowDescriptorTypeTextBaseCell title:self.label];
        [row setValueTransformer:[FormRowValueTrasformer class]];
    } else if ([self.type isEqualToString:@"textarea"]) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:newString rowType:XLFormRowDescriptorTypeTextAreaCell title:self.label];
        [row setValueTransformer:[FormRowValueTrasformer class]];
    } else if ([self.type isEqualToString:@"combobox"]) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:newString rowType:XLFormRowDescriptorTypeSelectorPush title:self.label];
        [row setValueTransformer:[FormRowValueTrasformer class]];
        [row.action setViewControllerStoryboardId:@"formRadio"];
    } else if ([self.type isEqualToString:@"radio"]) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:newString rowType:XLFormRowDescriptorTypeSelectorPush title:self.label];
        [row setValueTransformer:[FormRowValueTrasformer class]];
        [row.action setViewControllerStoryboardId:@"formRadio"];
    } else {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:newString rowType:XLFormRowDescriptorTypeSelectorPush title:self.label];
        [row setValueTransformer:[FormRowValueTrasformer class]];
        [row.action setViewControllerStoryboardId:@"form"];
    }
    
    if (condition) {
        [row setHidden:condition];
    }
    [row setValue:self];
    
    return row;
}

@end

@implementation FormModel

- (void)getForm {
    [[API sharedInstance] appApiRequest:FORM_METHOD isPost:NO parameters:nil success:^(id response) {
        NSError *error = nil;
        FormModel *responseObject = [[FormModel alloc] initWithDictionary:response error:&error];
        [[CurrentUser instance] setForm:responseObject];
        [[NSNotificationCenter defaultCenter] postNotificationName:FORM_MODEL_SUCCESS object:responseObject];
    } fail:^(NSString *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:FORM_MODEL_FAIL object:error];
    }];
}

- (NSArray *)getFormRowsByCondition:(NSString *)condition data:(NSArray<FormRow> *)data {
    NSMutableArray *rows = [NSMutableArray new];
    FormRow *r;
    NSArray *currentRowsData = (data) ? data : self.data;

    for (id currentFormRow in currentRowsData) {
        if ([currentFormRow isKindOfClass:[FormRow class]]) {
            r = currentFormRow;
            XLFormRowDescriptor *descriptor = [r getRowByCondition:condition];
            [rows addObject:descriptor];

            if ([r values]) {
                for (NSDictionary *v in [r values]) {
                    FormRow *cr = [[FormRow alloc] initWithDictionary:v error:nil];
                    
                    if ([cr.has_children boolValue]) {
                        [rows addObject:[self getFormRowsByCondition:[NSString stringWithFormat:@"NOT $%@.value.currentValue contains '%@'", [CurrentUser getTagString:[NSString stringWithFormat:@"%@", cr.cid]], cr.id] data:(id)cr.children]];
                    }
                }
            }
        }
        if ([currentFormRow isKindOfClass:[NSDictionary class]]) {
            r = [[FormRow alloc] initWithDictionary:currentFormRow error:nil];
            XLFormRowDescriptor *descriptor = [r getRowByCondition:condition];
            [rows addObject:descriptor];

            if ([r values]) {
                for (NSDictionary *v in [r values]) {
                    FormRow *cr = [[FormRow alloc] initWithDictionary:v error:nil];
                    
                    if ([cr.has_children boolValue]) {
                        [rows addObject:[self getFormRowsByCondition:[NSString stringWithFormat:@"NOT $%@.value.currentValue contains '%@'", [CurrentUser getTagString:[NSString stringWithFormat:@"%@", cr.cid]], cr.id] data:(id)cr.children]];
                    }
                }
            }
        }
    }
    
    return rows;
}

@end

@implementation FormRowOutputModel

- (void)setData :(FormRow *)data {
    [self setLabel:data.label];
    [self setControlId:data.id];
    [self setControlType:data.type];
    NSMutableArray *displayValues = [NSMutableArray new];
    for (NSString *tag in data.currentValue) {
        [displayValues addObject:[CurrentUser getValueByTag:tag data:nil value:@""]];
    }
    [self setValue:[displayValues componentsJoinedByString:@", "]];
}

@end

@implementation FormInputModel
@end

@implementation FormOutputModel

- (NSMutableArray<FormRowOutputModel> *)setFormData :(NSArray<FormRow> *)data formData:(NSDictionary *)formData {
    NSMutableArray *rows = [[NSMutableArray alloc] init];
    FormRow *r;
    FormRow *res;
    FormRow *cr;
    FormRowOutputModel *outputModel;
    NSArray *currentRowsData = (data) ? data : [[[CurrentUser instance] form] data];

    for (id dict in currentRowsData) {
        if ([dict  isKindOfClass:[NSDictionary class]]) {
            r = [[FormRow alloc] initWithDictionary:dict error:nil];
        } else {
            r = [[FormRow alloc] initWithDictionary:[dict toDictionary] error:nil];
        }
        
        res = [formData objectForKey:[CurrentUser getTagString:r.id]];
        
        outputModel = [[FormRowOutputModel alloc] init];
        [outputModel setData:res];
        
        if (r.values) {
            for (NSDictionary *v in [r values]) {
                cr = [[FormRow alloc] initWithDictionary:v error:nil];
                
                if ([cr.has_children boolValue]) {
                    [outputModel setChildren:[self setFormData:(id)cr.children formData:formData]];
                }
            }
        }
        
        [rows addObject:outputModel];
    }
    
    return (id)rows;
}

- (void)request:(void (^)(FormInputModel *))success failure:(void (^)(NSString *))failure {
    [[API sharedInstance] appApiRequest:FORM_METHOD_ORDER isPost:YES parameters:self success:^(id response) {
        NSLog(@"input data: %@", response);
//        FormInputModel *inputModel = [[FormInputModel alloc] initWithDictionary:response error:nil];
//        success(inputModel);
    } fail:^(NSString *error) {
        failure(error);
    }];
}

@end
