//
//  API.h
//  Comasco
//
//  Created by Данияр Габбасов on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <JSONModel/JSONModel.h>

@interface API : NSObject

+ (API*)sharedInstance;

- (void)appApiRequest :(NSString *)method isPost:(bool)isPost parameters:(JSONModel *)parameters success:(void (^)(id response))success fail:(void (^)(NSString *error))fail;

@end
