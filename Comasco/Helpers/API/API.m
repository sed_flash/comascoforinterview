//
//  API.m
//  Comasco
//
//  Created by Данияр Габбасов on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "API.h"

#define apiUrl @"http://comasco.aisystems.ru/api/1.0/"
#define defaultErrorString @"Ошибка интернет соединения..."

@implementation API

static API *appApi;

+ (API*)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appApi = [[API alloc] init];
    });
    
    return appApi;
}

- (void)appApiRequest :(NSString *)method isPost:(bool)isPost parameters:(JSONModel *)parameters success:(void (^)(id response))success fail:(void (^)(NSString *error))fail {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", apiUrl, method]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSLog(@"url: %@", url);
    if (isPost) {
        [manager POST:url.absoluteString parameters:parameters.toDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            fail(defaultErrorString);
        }];
    } else {
        [manager GET:url.absoluteString parameters:parameters.toDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            success(responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            fail(defaultErrorString);
        }];
    }
    return;
}

@end
