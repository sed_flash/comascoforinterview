//
//  CurrentUser.m
//  stockApp
//
//  Created by Данияр Габбасов on 10.06.16.
//  Copyright © 2016 northalliance. All rights reserved.
//

#import "CurrentUser.h"

@implementation CurrentUser {
    NSUserDefaults *_userDefaults;
}

#pragma mark - ROOT

- (instancetype)init {
    NSAssert(0, @"Use 'instance'");
    
    return nil;
}

- (instancetype)initPrivate {
    _userDefaults = [[NSUserDefaults alloc] init];
    _userDefaults = [NSUserDefaults standardUserDefaults];
    [_userDefaults synchronize];
    
    return [super init];
}

+ (NSString *)getTagString :(NSString *)string {
    return [NSString stringWithFormat:@"tag%@", [string stringByReplacingOccurrencesOfString:@"-" withString:@""]];
}

+ (NSString *)getValueByTag:(NSString *)tag data:(NSArray<FormRow> *)data value:(NSString *)value {
    FormRow *r;
    NSArray *currentRowsData = (data) ? data : [[[CurrentUser instance] form] data];
    
    for (id currentFormRow in currentRowsData) {
        if ([currentFormRow isKindOfClass:[FormRow class]]) {
            r = currentFormRow;
            
            if ([r.id isEqualToString:tag]) {
                value = r.value_text;
            }
            
            if ([r values]) {
                for (NSDictionary *v in [r values]) {
                    FormRow *cr = [[FormRow alloc] initWithDictionary:v error:nil];
                    
                    if ([cr.id isEqualToString:tag]) {
                        value = cr.value_text;
                    }
                    
                    if ([cr.has_children boolValue]) {
                        value = [CurrentUser getValueByTag:tag data:(id)cr.children value:value];
                    }
                }
            }
        }
        if ([currentFormRow isKindOfClass:[NSDictionary class]]) {
            r = [[FormRow alloc] initWithDictionary:currentFormRow error:nil];
            
            if ([r.id isEqualToString:tag]) {
                return r.value_text;
            }
            
            if ([r values]) {
                for (NSDictionary *v in [r values]) {
                    FormRow *cr = [[FormRow alloc] initWithDictionary:v error:nil];
                    
                    if ([cr.id isEqualToString:tag]) {
                        value = cr.value_text;
                    }
                    
                    if ([cr.has_children boolValue]) {
                        value = [CurrentUser getValueByTag:tag data:(id)cr.children value:value];
                    }
                }
            }
        }
    }
    
    return value;
}

#pragma mark - Singleton

+ (instancetype)instance {
    static CurrentUser *currentUser;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentUser = [[[self class] alloc] initPrivate];
    });
    
    return currentUser;
}

#pragma mark - Properties

- (void)setForm:(FormModel *)form {
    [_userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:form] forKey:@"form"];
    [_userDefaults synchronize];
}

- (FormModel *)form {
    if ([[_userDefaults objectForKey:@"form"] isKindOfClass:[NSData class]]) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:[_userDefaults objectForKey:@"form"]];
    }
    
    return nil;
}

@end
