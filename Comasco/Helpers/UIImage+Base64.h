//
//  UIImage+Base64.h
//  stockApp
//
//  Created by Данияр Габбасов on 13.07.16.
//  Copyright © 2016 northalliance. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Base64)

- (NSString *)base64String;

@end
