//
//  UINavigationItem+CElements.m
//  Comasco
//
//  Created by Vlad on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "UINavigationItem+CElements.h"

@implementation UINavigationItem (CElements)

- (void)setMenuButtonWithTarget :(id)target action:(SEL)action {
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    
    if (IS_IPHONE) {
        [menuButton setFrame:CGRectMake(0, 0, WidthWithPercent(7.5), HeightWithPercent(3.4))];
    } else /* iPad */{
        [menuButton setFrame:CGRectMake(0, 0, 40, 32)];
    }
    
    [menuButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [self setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:menuButton]];
}

@end
