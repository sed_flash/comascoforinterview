//
//  CTextField.h
//  Comasco
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTextField : UITextField <UITextFieldDelegate>

@property (assign, nonatomic, getter=isEmail) BOOL email;

- (void)setPlaceholderText:(NSString *)placeholderText;
- (void)setHiddenAlertView:(BOOL)hidden;
- (BOOL)isHiddenAlertView;
+ (BOOL)isValidTextFieldsInView:(UIView *)view;
+ (BOOL)isCorrectEmail:(CTextField *)textField;

@end
