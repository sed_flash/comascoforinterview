//
//  CMainScreenButton.m
//  Comasco
//
//  Created by Vlad on 08.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "CMainScreenButton.h"

@implementation CMainScreenButton


#pragma mark - Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
    
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self settings];
}

#pragma mark - Settings

- (void)settings {
    [self setBackgroundColor:[UIColor brownColor]];
}

@end
