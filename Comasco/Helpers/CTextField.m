//
//  CTextField.m
//  Comasco
//
//  Created by Vlad on 25.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "CTextField.h"

#define placeholderColor RGBColor(0x575757, 1)

@implementation CTextField {
    UIImageView *alertView;
}

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self settings];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self settings];
    self.delegate = self;
}

#pragma mark - Settings

- (void)settings {
    [self.layer setBorderColor:RGBColor(0x575757, 1).CGColor];
    [self.layer setBorderWidth:1.0f];
    [self.layer setCornerRadius:2.0f];
    [self.layer setMasksToBounds:YES];
    [self setFont:[UIFont fontWithName:@"Arial" size:12.8f]];
    [self setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    alertView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [alertView setImage:[UIImage imageNamed:@"warning"]];
    alertView.hidden = YES;
    
    [self addSubview:alertView];
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [alertView setCenter:CGPointMake(CGRectGetWidth(self.frame) - CGRectGetWidth(alertView.frame) / 2 - 10,
                                     CGRectGetHeight(self.frame) / 2)];
}

- (void)setPlaceholderText:(NSString *)placeholderText {
    if ([self respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = placeholderColor;
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholderText attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
}

#pragma makr - Behavior

+ (BOOL)isValidTextFieldsInView:(UIView *)view {
    BOOL flag = YES;
    for (UIView *v in [view subviews]) {
        if ([v isKindOfClass:[CTextField class]]) {
            CTextField *temp = (CTextField *)v;
            if ([@"" isEqualToString:temp.text]) {
                [temp setHiddenAlertView:NO];
                flag = NO;
            }
            if (temp.isEmail) {
                if (![self isCorrectEmail:temp]) {
                    [temp setHiddenAlertView:NO];
                    flag = NO;
                }
            }
        }
    }
    
    return flag;
}

+ (BOOL)isCorrectEmail:(CTextField *)textField {
    NSString *regexp = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regexp];
    if ([predicate evaluateWithObject:[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]) {
        [textField setHiddenAlertView:YES];
        return YES;
    }
    return NO;
}

#pragma mark - Text Fields Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([self isEmail]) {
        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    [self setHiddenAlertView:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
//    if ([textField.text isEqualToString:@""]) {
//        [self setHiddenAlertView:NO];
//    } else if ([self isEmail]) {
//        if (![CTextField isCorrectEmail:self]) {
//            [self setHiddenAlertView:NO];
//        }
//    }
    
    [textField resignFirstResponder];
}

#pragma mark - Setters

- (void)setHiddenAlertView:(BOOL)hidden {
    if (!hidden)
        [self.layer setBorderColor:RGBColor(0xe04006, 1).CGColor];
    else
        [self.layer setBorderColor:RGBColor(0x575757, 1).CGColor];
    [alertView setHidden:hidden];
}

#pragma mark - Getters

- (BOOL)isHiddenAlertView {
    return alertView.hidden;
}

- (BOOL)isEmail {
    return _email;
}

@end
