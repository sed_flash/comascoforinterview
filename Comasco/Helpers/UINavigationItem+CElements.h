//
//  UINavigationItem+CElements.h
//  Comasco
//
//  Created by Vlad on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (CElements)
- (void)setMenuButtonWithTarget :(id)target action:(SEL)action;
@end
