//
//  Constants.h
//  Comasco
//
//  Created by Vlad on 21.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kCallURL;
FOUNDATION_EXPORT NSString *const kSecondURL;
FOUNDATION_EXPORT NSString *const kMachinesURL;

FOUNDATION_EXPORT NSString *const kFirstKey;
FOUNDATION_EXPORT NSString *const kSecondKey;
FOUNDATION_EXPORT NSString *const kThirdKey;