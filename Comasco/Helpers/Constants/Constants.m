//
//  Constants.m
//  Comasco
//
//  Created by Vlad on 21.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "Constants.h"

NSString *const kCallURL = @"http://www.comasco.co.il";
NSString *const kSecondURL = @"http://www.comasco.co.il/%D7%9B%D7%9C%D7%99%D7%9D-%D7%99%D7%93-%D7%A9%D7%A0%D7%99%D7%94-%D7%97%D7%93%D7%A9.html";
NSString *const kMachinesURL = @"http://www.comasco.co.il/162873/katalog-cellular";

NSString *const kFirstKey = @"first";
NSString *const kSecondKey = @"second";
NSString *const kThirdKey = @"third";
