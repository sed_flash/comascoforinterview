//
//  WebPreloader.m
//  Comasco
//
//  Created by Vlad on 21.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "WebPreloader.h"

@implementation WebPreloader

- (instancetype)init {
    NSAssert(0, @"Use 'sharedInstance'");
    return nil;
}

+ (WebPreloader *)sharedInstance {
    static WebPreloader *instante = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instante = [[[self class] alloc] initPrivate];
    });
    
    return instante;
}

- (id)initPrivate {
    self.preloader = [[FISWebViewPreloader alloc] initWithCapacity:8 scheduleType:FIFO];
    
    return [super init];
}

@end
