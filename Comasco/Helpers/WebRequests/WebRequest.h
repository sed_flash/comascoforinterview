//
//  WebRequest.h
//  Comasco
//
//  Created by Vlad on 01.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WebRequest : NSObject

+ (WebRequest *)sharedInstance;

- (void)doRequestToResource:(NSString *)link
                  onWebView:(UIWebView *)webView
                     forKey:(NSString *)key
                    ;

- (void)doRequestToResource:(NSString *)link
                  onWebView:(UIWebView *)webView;

@end
