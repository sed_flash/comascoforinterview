//
//  WebRequest.m
//  Comasco
//
//  Created by Vlad on 01.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "WebRequest.h"

@implementation WebRequest

+ (WebRequest *)sharedInstance {
    static WebRequest *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[WebRequest alloc] init];
    });
    
    return instance;
}

- (void)doRequestToResource:(NSString *)link
                         onWebView:(UIWebView *)webView
                            forKey:(NSString *)key
                            {
    
    NSURL *url = [NSURL URLWithString:link];
    NSURLRequest *requst = [NSURLRequest requestWithURL:url
                                            cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                        timeoutInterval:30];
    if (key) {
        webView = [[WebPreloader sharedInstance].preloader webViewForKey:key];
        if (![self connected] || webView == nil) {
            webView = [self makeWebView];
            [webView loadRequest:requst];
        }
    } else {
        
        if (!webView) {
            webView = [self makeWebView];
        }

        [webView loadRequest:requst];
    }
    webView.scrollView.bounces = NO;
    
//    success(webView);
}

- (void)doRequestToResource:(NSString *)link
                  onWebView:(UIWebView *)webView {
    
    NSURL *url = [NSURL URLWithString:link];
    NSURLRequest *requst = [NSURLRequest requestWithURL:url
                                            cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                        timeoutInterval:30];
    [webView loadRequest:requst];
}

- (UIWebView *)makeWebView {
    CGRect screen = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    UIWebView *webView = [[UIWebView alloc] initWithFrame:screen];
    [webView setScalesPageToFit:YES];
    
    return webView;
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
