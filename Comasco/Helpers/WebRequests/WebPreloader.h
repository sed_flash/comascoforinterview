//
//  WebPreloader.h
//  Comasco
//
//  Created by Vlad on 21.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FISWebViewPreloader.h"

@interface WebPreloader : UIWebView

+ (WebPreloader *)sharedInstance;
@property (strong,nonatomic) FISWebViewPreloader *preloader;

@end
