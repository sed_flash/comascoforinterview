//
//  PreloaderViewController.m
//  Comasco
//
//  Created by Vlad on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "PreloaderViewController.h"

@interface PreloaderViewController()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;

@end

@implementation PreloaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestResponse:) name:FORM_MODEL_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestResponse:) name:FORM_MODEL_FAIL object:nil];
    
    [self request];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - User Methods

- (void)request {
    [self.retryButton setHidden:YES];
    FormModel *form = [[FormModel alloc] init];
    [form getForm];
}

- (void)requestResponse: (NSNotification *)notification {
    if ([notification.name isEqualToString:FORM_MODEL_SUCCESS]) {
        [self.view.window setRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"initial"]];
    } else {
        [self.retryButton setHidden:NO];
    }
}

- (IBAction)retryAction:(id)sender {
    [self request];
}

@end
