//
//  CallViewController.m
//  Comasco
//
//  Created by Vlad on 23.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "CallViewController.h"
#import "UINavigationItem+CElements.h"

#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface CallViewController ()

//@property (strong,nonatomic) UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation CallViewController {
    Reachability *internetReachability;
}

#pragma mark - View's methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setMenuButtonWithTarget:self action:@selector(menuButtonTapped)];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    [self requestToResource];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
}

- (void)requestToResource {
    _webView.scrollView.bounces = NO;
    
    [[WebRequest sharedInstance] doRequestToResource:kCallURL onWebView:self.webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)menuButtonTapped {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

- (void)dealloc {
    [self.webView cleanForDealloc];
    self.webView = nil;
}

@end
