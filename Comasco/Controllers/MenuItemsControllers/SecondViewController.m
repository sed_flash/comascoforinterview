//
//  SecondViewController.m
//  Comasco
//
//  Created by Vlad on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "SecondViewController.h"
#import "UINavigationItem+CElements.h"

#import "UIViewController+ECSlidingViewController.h"
#import "ECSlidingViewController.h"

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    
    [self.navigationItem setMenuButtonWithTarget:self action:@selector(menuButtonTapped)];
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];

}

- (void)menuButtonTapped {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

@end
