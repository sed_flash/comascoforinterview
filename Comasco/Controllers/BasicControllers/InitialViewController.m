//
//  InitialViewController.m
//  Comasco
//
//  Created by Vlad on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "InitialViewController.h"
#import "MainNavigationController.h"
#import "ViewController.h"

@implementation InitialViewController

- (void)viewDidLoad {
    self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CMainScreenNavigationController"];
    
//    MainNavigationController *mnc = [self.storyboard instantiateViewControllerWithIdentifier:@"CMainScreenNavigationController"];
//    
//    self.topViewController = mnc;
    
    [super viewDidLoad];
}

@end
