//
//  ViewController.m
//  Comasco
//
//  Created by Vlad on 23.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "ViewController.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "ECSlidingViewController.h"
#import "UINavigationItem+CElements.h"
#import "CMainScreenButton.h"

@interface ViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet CMainScreenButton *requestBtn;
@property (weak, nonatomic) IBOutlet CMainScreenButton *warehouseBtn;
@property (weak, nonatomic) IBOutlet CMainScreenButton *instrumentsBtn;
@property (weak, nonatomic) IBOutlet CMainScreenButton *stuffBtn;

@end

@implementation ViewController
#pragma mark - View's method

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
    }
    
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures = @[];
    
    [self.navigationItem setMenuButtonWithTarget:self action:@selector(menuButtonTapped)];
    [self request];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self preloadWebViews];
    [self configureNavigationBar];
}

- (void)configureNavigationBar {
    [[[self navigationController] navigationBar] setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

#pragma mark - User Methods

- (void)preloadWebViews {
    CGRect viewRect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [[WebPreloader sharedInstance].preloader setURLString:kCallURL
                                                   forKey:kFirstKey
                                               withCGRect:viewRect];
    [[WebPreloader sharedInstance].preloader setURLString:kSecondURL
                                                   forKey:kSecondKey
                                               withCGRect:viewRect];
    [[WebPreloader sharedInstance].preloader setURLString:kMachinesURL
                                                   forKey:kThirdKey
                                               withCGRect:viewRect];
}

- (void)request {
    FormModel *form = [[FormModel alloc] init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [form getForm];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menuButtonTapped {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

@end
