//
//  MenuViewController.h
//  Comasco
//
//  Created by Vlad on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@end
