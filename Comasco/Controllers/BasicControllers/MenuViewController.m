//
//  MenuViewController.m
//  Comasco
//
//  Created by Vlad on 18.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "MenuViewController.h"
#import "CallViewController.h"

@interface MenuViewController ()

@property (nonatomic, strong) NSString *lastItem;
@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) UINavigationController *transitionsNavigationController;
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (IS_IPHONE) {
        [self.slidingViewController setAnchorRightRevealAmount:WidthWithPercent(85)];
    } else /* iPad */{
        [self.slidingViewController setAnchorRightRevealAmount:WidthWithPercent(45)];
    }
    
    _menuTableView.dataSource = self;
    _menuTableView.delegate = self;
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    [v setBackgroundColor:[UIColor brownColor]];
    [self.view addSubview:v];
    
    [_menuTableView setContentInset:UIEdgeInsetsMake(100, 0, 0, 0)];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

#pragma mark - Properties

- (NSArray *)menuItems {
    if (_menuItems) return _menuItems;
    
    _menuItems = @[@"Home",
                   @"First",
                   @"Second",
                   @"Third",
                   @"Thourd",
                   @"Last"];
    
    return _menuItems;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSString *menuItem = self.menuItems[indexPath.row];
    
    cell.textLabel.text = menuItem;
    [cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO];
    
    NSString *menuItem = self.menuItems[indexPath.row];
    NSString *path;
    
    if (![@"" isEqualToString:menuItem]) {
        
        path = [self navagationIdentifier:menuItem];
        
        if (![menuItem isEqualToString:_lastItem]) {
            self.transitionsNavigationController = nil;
        }
        
        if (self.transitionsNavigationController == nil || [menuItem isEqualToString:@"Home"]) {
            
            self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:path];
            self.transitionsNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
            
        } else {
            self.slidingViewController.topViewController = self.transitionsNavigationController;
        }
        
        _lastItem = menuItem;
        [self.slidingViewController resetTopViewAnimated:YES];
        
    }
}

- (NSString *)navagationIdentifier:(NSString *)menuItem {
    
    if ([menuItem isEqualToString:@"Home"]) {
        return @"CMainScreenNavigationController";
        
    } else if ([menuItem isEqualToString:@"First"]) {
        return @"CallNavigationController";
        
    } else if ([menuItem isEqualToString:@"Second"]) {
        return @"PhotoNavigationController";
        
    } else if ([menuItem isEqualToString:@"Third"]) {
        return @"OneManNavigationController";
        
    } else if ([menuItem isEqualToString:@"Thourd"]) {
        return @"TwoManNavigationController";
        
    } else {
        return @"ShareNavigationController";
    }
    
}

@end
