//
//  LogInView.h
//  Comasco
//
//  Created by Vlad on 24.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogInView : UIView

@property (nonatomic, weak) UIViewController *viewController;

@end
