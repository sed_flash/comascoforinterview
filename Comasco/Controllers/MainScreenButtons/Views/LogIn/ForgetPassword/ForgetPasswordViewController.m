//
//  ForgetPasswordViewController.m
//  Comasco
//
//  Created by Vlad on 30.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "ForgetPasswordViewController.h"

@interface ForgetPasswordViewController ()

@property (weak, nonatomic) IBOutlet UIButton *sendNewEmailButton;
@property (weak, nonatomic) IBOutlet CTextField *textField;

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateTitleAllElements];
    [_textField setEmail:YES];
    [_textField setPlaceholderText:NSLocalizedString(@"email", nil)];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Action Methods

- (IBAction)sendNewEmailAction:(id)sender {
    if (![CTextField isValidTextFieldsInView:self.view]) {
        return;
    }
}

- (IBAction)closeView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Touches

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

#pragma mark - Elements settings

- (void)updateTitleAllElements {
    [_textField setPlaceholderText:NSLocalizedString(@"email", nil)];
    [_sendNewEmailButton setTitle:NSLocalizedString(@"send_password", nil) forState:UIControlStateNormal];
}


@end
