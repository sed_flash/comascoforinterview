//
//  LogInView.m
//  Comasco
//
//  Created by Vlad on 24.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "LogInView.h"
#import "ForgetPasswordViewController.h"

@interface LogInView ()

@property (weak, nonatomic) IBOutlet UIButton *forgetPasswordButton;
@property (weak, nonatomic) IBOutlet CTextField *emailTextField;
@property (weak, nonatomic) IBOutlet CTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *logInBtn;
@property (weak, nonatomic) IBOutlet UILabel *rememberLabel;

@end

@implementation LogInView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self updateTitleAllElements];
    [_emailTextField setEmail:YES];

    [_passwordTextField setSecureTextEntry:YES];
}

#pragma mark - Action methods

- (IBAction)LogInAction:(id)sender {
    if (![CTextField isValidTextFieldsInView:self]) {
        return;
    }
    
}

- (IBAction)forgetPasswordAction:(id)sender {
    ForgetPasswordViewController *fpvc = [_viewController.view.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"newEmailController"];
    NSAssert(_viewController, @"The View Controller in LogInView is nil!");
    [_viewController presentViewController:fpvc animated:NO completion:nil];
}

#pragma mark - Elements settings

- (void)updateTitleAllElements {
    [_emailTextField setPlaceholderText:NSLocalizedString(@"email", nil)];
    [_passwordTextField setPlaceholderText:NSLocalizedString(@"password", nil)];
    [_rememberLabel setText:NSLocalizedString(@"remember", nil)];
    [_forgetPasswordButton setTitle:NSLocalizedString(@"forget", nil) forState:UIControlStateNormal];
    [_logInBtn setTitle:NSLocalizedString(@"log_in", nil) forState:UIControlStateNormal];
}

@end
