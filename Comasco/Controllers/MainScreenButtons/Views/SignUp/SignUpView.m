//
//  SignUpView.m
//  Comasco
//
//  Created by Vlad on 24.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "SignUpView.h"

@interface SignUpView ()

@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet CTextField *nameTextField;
@property (weak, nonatomic) IBOutlet CTextField *emailTextField;
@property (weak, nonatomic) IBOutlet CTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet CTextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UILabel *rememberLabel;

@end

@implementation SignUpView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self updateTitleAllElements];
    [_emailTextField setEmail:YES];
    [_passwordTextField setSecureTextEntry:YES];
    [_confirmPasswordTextField setSecureTextEntry:YES];
}

#pragma mark - Action methods

- (IBAction)SignUpAction:(id)sender {
    if (![CTextField isValidTextFieldsInView:self]) {
//        return;
    }
    if (![_passwordTextField.text isEqualToString:_confirmPasswordTextField.text]) {
        [_confirmPasswordTextField setHiddenAlertView:NO];
    }
}

#pragma mark - Elements settings

- (void)updateTitleAllElements {
    [_nameTextField setPlaceholderText:NSLocalizedString(@"name", nil)];
    [_emailTextField setPlaceholderText:NSLocalizedString(@"email", nil)];
    [_passwordTextField setPlaceholderText:NSLocalizedString(@"password", nil)];
    [_confirmPasswordTextField setPlaceholderText:NSLocalizedString(@"conf_password", nil)];
    [_rememberLabel setText:NSLocalizedString(@"remember", nil)];
    [_signUpBtn setTitle:NSLocalizedString(@"sign_up", nil) forState:UIControlStateNormal];
}


@end
