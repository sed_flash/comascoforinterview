//
//  InstrumentsViewController.m
//  Comasco
//
//  Created by Vlad on 02.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "InstrumentsViewController.h"
#import "SecondHandMachinesViewController.h"
#import "MachinesViewController.h"

@interface InstrumentsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *secondMachinesLabel;
@property (weak, nonatomic) IBOutlet UILabel *machinesLabel;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *machineGesture;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *secondHandMachineGesture;

@end

@implementation InstrumentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupNavigationController];
}

- (void)setupNavigationController {
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions methods 

- (IBAction)secondMachineAction:(id)sender {
    SecondHandMachinesViewController *scmvc = [self.view.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"secondHandMachines"];
    [self.navigationController pushViewController:scmvc animated:YES];
}

- (IBAction)machineAction:(id)sender {
    MachinesViewController *mvc = [self.view.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"machinesController"];
    [self.navigationController pushViewController:mvc animated:YES];
}

@end
