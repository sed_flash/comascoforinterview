//
//  SecondHandMachinesViewController.m
//  Comasco
//
//  Created by Vlad on 02.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "SecondHandMachinesViewController.h"

@interface SecondHandMachinesViewController ()

//@property (weak, nonatomic) UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation SecondHandMachinesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self requestToResource];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupNavigationController];
}

- (void)setupNavigationController {
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)requestToResource {
    _webView.scrollView.bounces = NO;
    
    [[WebRequest sharedInstance] doRequestToResource:kSecondURL onWebView:_webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.webView cleanForDealloc];
    self.webView = nil;
}

@end
