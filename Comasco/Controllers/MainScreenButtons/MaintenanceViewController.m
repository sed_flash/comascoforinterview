//
//  MaintenanceViewController.m
//  Comasco
//
//  Created by Vlad on 01.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "MaintenanceViewController.h"

#define kPainting @"kPainting"

@implementation MaintenanceViewController {
    NSMutableArray *tableRows;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures = @[];
    
    tableRows = [NSMutableArray new];
    
    [self tableInit:[[[CurrentUser instance] form] getFormRowsByCondition:nil data:nil]];
    [self initForm];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)initForm {
    XLFormDescriptor *form = [XLFormDescriptor formDescriptor];
    XLFormSectionDescriptor *section = [XLFormSectionDescriptor formSection];
    XLFormRowDescriptor *row;
    
    if (!IS_IPHONE) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:kPainting rowType:XLFormRowDescriptorTypeDrawCell title:@"Painting"];
        [row.action setFormSegueIdentifier:@"painting"];
        [section addFormRow:row];
    }
    
    for (id currentRow in tableRows) {
        [section addFormRow:currentRow];
    }
    [form addFormSection:section];
    
    section = [XLFormSectionDescriptor formSection];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:nil rowType:XLFormRowDescriptorTypeButtonCell title:@"Submit"];
    [row.action setFormSelector:@selector(submit)];
    [section addFormRow:row];
    
    [form addFormSection:section];
    [self setForm:form];
}

- (void)tableInit :(NSArray *)array {
    for (id r in array) {
        if ([r isKindOfClass:[XLFormRowDescriptor class]]) {
            [tableRows addObject:r];
        } else if ([r isKindOfClass:[NSArray class]]) {
            [self tableInit:r];
        }
    }
}

#pragma mark - SUCCESS

- (void)submit {
    FormOutputModel *formModel = [[FormOutputModel alloc] init];
    if ([[self.formValues objectForKey:kPainting] isKindOfClass:[UIImage class]]) {
        UIImage *paintImage = [self.formValues objectForKey:kPainting];
        [formModel setSignature:[paintImage base64String]];
    }
    [formModel setOrderData:[formModel setFormData:nil formData:self.formValues]];
    
    [formModel request:^(FormInputModel *inputModel) {
        NSLog(@"inputModel: %@", inputModel.toDictionary);
    } failure:^(NSString *errorMessage) {
        NSLog(@"error: %@", errorMessage);
    }];
}

@end
