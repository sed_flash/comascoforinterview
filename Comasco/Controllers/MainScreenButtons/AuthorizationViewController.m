//
//  AuthorizationViewController.m
//  Comasco
//
//  Created by Vlad on 23.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "AuthorizationViewController.h"
#import "SignUpView.h"
#import "LogInView.h"

#define SIGN_UP_SEGMENT 0
#define LOG_IN_SEGMENT  1

@interface AuthorizationViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentSwitcher;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@end

@implementation AuthorizationViewController {
    SignUpView *_signUpView;
    LogInView *_logInView;
    BOOL theSame;
}

#pragma mark - View methods

- (void)viewDidLoad {
    [super viewDidLoad];

    [self updateTitleAllElements];
    _segmentSwitcher.selectedSegmentIndex = 0;
    
    _signUpView = [[NSBundle mainBundle] loadNibNamed:@"SignUpXib"
                                                owner:self
                                              options:nil][0];
    _logInView  = [[NSBundle mainBundle] loadNibNamed:@"LogInXib"
                                                owner:self
                                              options:nil][0];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_segmentSwitcher.selectedSegmentIndex == LOG_IN_SEGMENT)
        _signUpView.hidden = YES;
    _logInView.viewController = self;
    [self.view layoutIfNeeded];
    [_signUpView setFrame:CGRectMake(0, 0, CGRectGetWidth(_contentView.frame), CGRectGetHeight(_contentView.frame))];
    [_contentView addSubview:_signUpView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Another methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)removeSubviewsFromContentView {
    for (UIView *v in [_contentView subviews]) {
        [v removeFromSuperview];
    }
}

#pragma mark - Actions

- (IBAction)segmentValueChanged:(id)sender {
    if (_segmentSwitcher.selectedSegmentIndex == 0) {
        _signUpView.hidden = NO;
        [self removeSubviewsFromContentView];
        [self.view layoutIfNeeded];
        [_signUpView setFrame:CGRectMake(0, 0, CGRectGetWidth(_contentView.frame), CGRectGetHeight(_contentView.frame))];
        [_contentView addSubview:_signUpView];
        
    } else if (_segmentSwitcher.selectedSegmentIndex == 1) {
        [self removeSubviewsFromContentView];
        [self.view layoutIfNeeded];
        [_logInView setFrame:CGRectMake(0, 0, CGRectGetWidth(_contentView.frame), CGRectGetHeight(_contentView.frame))];
        [_contentView addSubview:_logInView];
    }
}

- (IBAction)cancelAuth:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Touches

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}

#pragma mark - Elements settings

- (void)updateTitleAllElements {
    [_segmentSwitcher setTitle:NSLocalizedString(@"sign_up", nil) forSegmentAtIndex:0];
    [_segmentSwitcher setTitle:NSLocalizedString(@"log_in", nil) forSegmentAtIndex:1];
}

@end
