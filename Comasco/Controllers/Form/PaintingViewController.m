//
//  PaintingViewController.m
//  Comasco
//
//  Created by Vlad on 27.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "PaintingViewController.h"

@interface PaintingViewController ()
@property (weak, nonatomic) IBOutlet MyDrawView *drawView;
@end


@implementation PaintingViewController

@synthesize rowDescriptor = _rowDescriptor;
@synthesize popoverController = __popoverController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_drawView setDelegate:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleDone target:self.drawView action:@selector(erase)]];
}

#pragma mark - DrawDelegate

- (void)didHandUp:(UIImage *)image {
    self.rowDescriptor.value = image;
}

@end
