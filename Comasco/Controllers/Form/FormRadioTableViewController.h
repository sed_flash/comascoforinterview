//
//  FormRadioTableViewController.h
//  Comasco
//
//  Created by Vlad on 23.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormRadioTableViewController : UITableViewController <XLFormRowDescriptorViewController, XLFormRowDescriptorPopoverViewController>

@end
