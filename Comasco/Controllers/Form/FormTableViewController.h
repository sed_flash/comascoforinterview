//
//  FormTableViewController.h
//  Comasco
//
//  Created by Vlad on 21.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormTableViewController : UITableViewController <XLFormRowDescriptorViewController, XLFormRowDescriptorPopoverViewController>

@end
