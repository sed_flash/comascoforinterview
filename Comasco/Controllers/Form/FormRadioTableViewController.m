//
//  FormRadioTableViewController.m
//  Comasco
//
//  Created by Vlad on 23.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "FormRadioTableViewController.h"

@implementation FormRadioTableViewController {
    FormRow *rowValue;
    NSMutableArray *valuesArray;
}

@synthesize rowDescriptor = _rowDescriptor;
@synthesize popoverController = __popoverController;

- (void)viewDidLoad {
    [super viewDidLoad];
    rowValue = self.rowDescriptor.value;
    valuesArray = (rowValue.currentValue)?[NSMutableArray arrayWithArray:rowValue.currentValue]:[NSMutableArray new];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return rowValue.values.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    FormRow *currentRow = [[FormRow alloc] initWithDictionary:[rowValue.values objectAtIndex:indexPath.row] error:nil];
    [cell.textLabel setText:currentRow.value_text];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([valuesArray containsObject:currentRow.id]) {
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FormRow *currentRow = [[FormRow alloc] initWithDictionary:[rowValue.values objectAtIndex:indexPath.row] error:nil];
    
    if ([valuesArray containsObject:currentRow.id]) {
        [valuesArray removeObject:currentRow.id];
    } else {
        [valuesArray removeAllObjects];
        [valuesArray addObject:currentRow.id];
    }
    
    rowValue.currentValue = valuesArray;
    
    self.rowDescriptor.value = rowValue;
    
    [self.tableView reloadData];
}

@end
