//
//  TextAreaCell.m
//  Comasco
//
//  Created by Данияр Габбасов on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "TextAreaCell.h"

NSString * const XLFormRowDescriptorTypeTextAreaCell = @"XLFormRowDescriptorTypeTextAreaCell";

@interface TextAreaCell() <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextView *valueTextView;

@end

@implementation TextAreaCell {
    FormRow *currentRow;
}

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([TextAreaCell class]) forKey:XLFormRowDescriptorTypeTextAreaCell];
}

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.valueTextView.layer setBorderWidth:1.0f];
    [self.valueTextView.layer setBorderColor:RGBColor(0x000000, 1).CGColor];
    [self.valueTextView setDelegate:self];
}
-(void)update
{
    [super update];
    
    currentRow = self.rowDescriptor.value;
    [self.titleLabel setText:self.rowDescriptor.title];
    [self.valueTextView setText:[currentRow.currentValue firstObject]];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 147.0f;
}

- (void)setHidden:(BOOL)hidden {
    self.rowDescriptor.value = [NSNull new];
    [super setHidden:hidden];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    [self saveChanges:textView];
}

#pragma mark - UserMethods

- (void)saveChanges :(UITextView *)textView {
    NSMutableArray *valueArray = [NSMutableArray new];
    [valueArray addObject:textView.text];
    currentRow.currentValue = valueArray;
    
    self.rowDescriptor.value = currentRow;
}

@end
