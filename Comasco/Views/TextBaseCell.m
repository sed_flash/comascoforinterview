//
//  TextBaseCell.m
//  Comasco
//
//  Created by Данияр Габбасов on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "TextBaseCell.h"

NSString * const XLFormRowDescriptorTypeTextBaseCell = @"XLFormRowDescriptorTypeTextBaseCell";

@interface TextBaseCell() <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;
@end

@implementation TextBaseCell {
    FormRow *currentRow;
}

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([TextBaseCell class]) forKey:XLFormRowDescriptorTypeTextBaseCell];
}

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.valueTextField setDelegate:self];
    [self.valueTextField addTarget:self action:@selector(saveChanges:) forControlEvents:UIControlEventEditingChanged];
}

-(void)update
{
    [super update];
    
    currentRow = self.rowDescriptor.value;
    [self.titleLabel setText:self.rowDescriptor.title];
    [self.valueTextField setText:[currentRow.currentValue firstObject]];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 72.0f;
}

- (void)setHidden:(BOOL)hidden {
    self.rowDescriptor.value = [NSNull new];
    [super setHidden:hidden];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self saveChanges:textField];
    
    return true;
}

#pragma mark - UserMethods

- (void)saveChanges :(UITextField *)textField {
    NSMutableArray *valueArray = [NSMutableArray new];
    [valueArray addObject:textField.text];
    currentRow.currentValue = valueArray;
    
    self.rowDescriptor.value = currentRow;
}

@end
