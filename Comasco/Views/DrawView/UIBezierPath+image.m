//
//  UIBezierPath+image.m
//  test
//
//  Created by Vlad on 20.07.16.
//  Copyright © 2016 com.example. All rights reserved.
//

#import "UIBezierPath+image.h"
#define kCoefficient 20.0
@implementation UIBezierPath (image)

-(UIImage*) strokeImageWithColor:(UIColor*)color {
    CGRect bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width+kCoefficient, self.bounds.size.height+kCoefficient);
    
    // create a view to draw the path in
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width+kCoefficient, self.bounds.size.height+kCoefficient)];
    
    // begin graphics context for drawing
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, [[UIScreen mainScreen] scale]);
    
    // configure the view to render in the graphics context
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // get reference to the graphics context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // translate matrix so that path will be centered in bounds
    CGContextTranslateCTM(context, -(bounds.origin.x - self.lineWidth), -(bounds.origin.y - self.lineWidth));
    
    // set color
    [color set];
    
    // draw the stroke
    [self stroke];
    
    // get an image of the graphics context
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end the context
    UIGraphicsEndImageContext();
    
    return viewImage;
}
@end
