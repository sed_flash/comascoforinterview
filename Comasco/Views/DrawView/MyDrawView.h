//
//  MyDrawView.h
//  test
//
//  Created by Vlad on 20.07.16.
//  Copyright © 2016 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DrawDelegate
@required
- (void)didHandUp :(UIImage *)image;
@end

@interface MyDrawView : UIView
- (void)erase;
@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, strong) id<DrawDelegate> delegate;
@end
