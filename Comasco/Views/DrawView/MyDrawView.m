//
//  MyDrawView.m
//  test
//
//  Created by Vlad on 20.07.16.
//  Copyright © 2016 com.example. All rights reserved.
//

#import "MyDrawView.h"

@implementation MyDrawView {
}

- (void)commonInit {
    _path = [UIBezierPath bezierPath];
    [_path setLineWidth:2.0f];
    
    // Capture touches
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    pan.maximumNumberOfTouches = pan.minimumNumberOfTouches = 1;
    [self addGestureRecognizer:pan];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) [self commonInit];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) [self commonInit];
    return self;
}

- (void)erase {
    _path = [UIBezierPath bezierPath];
    [_path setLineWidth:2.0f];
    [self setNeedsDisplay];
}

- (void)pan:(UIPanGestureRecognizer *)pan {
    CGPoint currentPoint = [pan locationInView:self];
    
    if (pan.state == UIGestureRecognizerStateBegan) {
        [_path moveToPoint:currentPoint];
    } else if (pan.state == UIGestureRecognizerStateChanged) {
        [_path addLineToPoint:currentPoint];
    } else if (pan.state == UIGestureRecognizerStateEnded) {
        [self.delegate didHandUp:[_path strokeImageWithColor:RGBColor(0x000000, 1)]];
    }
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    [[UIColor blackColor] setStroke];
    [_path stroke];
}


@end
