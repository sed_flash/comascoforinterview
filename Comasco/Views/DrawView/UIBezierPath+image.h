//
//  UIBezierPath+image.h
//  test
//
//  Created by Vlad on 20.07.16.
//  Copyright © 2016 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBezierPath (image)
-(UIImage*) strokeImageWithColor:(UIColor*)color ;
@end
