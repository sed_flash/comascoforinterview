//
//  DrawCell.h
//  Comasco
//
//  Created by Данияр Габбасов on 26.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <XLForm/XLForm.h>

extern NSString * const XLFormRowDescriptorTypeDrawCell;

@interface DrawCell : XLFormBaseCell

@end
