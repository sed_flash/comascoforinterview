//
//  RadioCell.m
//  Comasco
//
//  Created by Данияр Габбасов on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "RadioCell.h"

NSString * const XLFormRowDescriptorTypeRadioCell = @"XLFormRowDescriptorTypeRadioCell";

@implementation RadioCell

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([XLFormRowDescriptorTypeRadioCell class]) forKey:XLFormRowDescriptorTypeRadioCell];
}

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}
-(void)update
{
    [super update];
    
    if ([self.rowDescriptor.value isKindOfClass:[FormRow class]]) {
        FormRow *currentFormRow = self.rowDescriptor.value;
    }
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 54.0f;
}

@end
