//
//  ComboBoxCell.h
//  Comasco
//
//  Created by Данияр Габбасов on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <XLForm/XLForm.h>

extern NSString * const XLFormRowDescriptorTypeComboBoxCell;

@interface ComboBoxCell : XLFormBaseCell

@end
