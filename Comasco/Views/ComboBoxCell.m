//
//  ComboBoxCell.m
//  Comasco
//
//  Created by Данияр Габбасов on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "ComboBoxCell.h"

NSString * const XLFormRowDescriptorTypeComboBoxCell = @"XLFormRowDescriptorTypeComboBoxCell";

@interface ComboBoxCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ComboBoxCell {
    FormRow *currentRow;
}

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([ComboBoxCell class]) forKey:XLFormRowDescriptorTypeComboBoxCell];
}

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}
-(void)update
{
    [super update];
    
    currentRow = self.rowDescriptor.value;
    [self.titleLabel setText:self.rowDescriptor.title];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 44.0f;
}

@end
