//
//  DrawCell.m
//  Comasco
//
//  Created by Данияр Габбасов on 26.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "DrawCell.h"

NSString * const XLFormRowDescriptorTypeDrawCell = @"XLFormRowDescriptorTypeDrawCell";

@interface DrawCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *paintingImage;

@end

@implementation DrawCell

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([DrawCell class]) forKey:XLFormRowDescriptorTypeDrawCell];
}

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
-(void)update
{
    [super update];

    [self.titleLabel setText:self.rowDescriptor.title];
    
    if ([self.rowDescriptor.value isKindOfClass:[UIImage class]]) {
        [self.paintingImage setImage:self.rowDescriptor.value];
    } else {
        [self.paintingImage setImage:nil];
    }
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 148.0f;
}

-(void)formDescriptorCellDidSelectedWithFormController:(XLFormViewController *)controller
{
    if (self.rowDescriptor.action.formSegueIdentifier){
        [controller performSegueWithIdentifier:self.rowDescriptor.action.formSegueIdentifier sender:self.rowDescriptor];
    }
}

@end
