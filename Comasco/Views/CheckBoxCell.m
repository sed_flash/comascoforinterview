//
//  CheckBoxCell.m
//  Comasco
//
//  Created by Данияр Габбасов on 19.07.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import "CheckBoxCell.h"

NSString * const XLFormRowDescriptorTypeCheckBoxCell = @"XLFormRowDescriptorTypeCheckBoxCell";

@interface CheckBoxCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchBox;

@end

@implementation CheckBoxCell {
    FormRow *currentRow;
    NSMutableArray *childRows;
}

+(void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:NSStringFromClass([CheckBoxCell class]) forKey:XLFormRowDescriptorTypeCheckBoxCell];
}

#pragma mark - XLFormDescriptorCell

- (void)configure
{
    [super configure];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    childRows = [NSMutableArray new];
    [self.switchBox addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
}

-(void)update
{
    [super update];
    
    currentRow = self.rowDescriptor.value;
    [self.titleLabel setText:self.rowDescriptor.title];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 45.0f;
}

- (void)valueChanged :(UISwitch *)currentSwitch {
    if (currentSwitch.on) {
        //on
    } else {
        //off
    }
}

@end
