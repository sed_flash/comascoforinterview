//
//  ButtonCell.h
//  stockApp
//
//  Created by Данияр Габбасов on 10.06.16.
//  Copyright © 2016 northalliance. All rights reserved.
//

#import <XLForm/XLForm.h>

extern NSString * const XLFormRowDescriptorTypeButtonCell;

@interface ButtonCell : XLFormBaseCell

@end
