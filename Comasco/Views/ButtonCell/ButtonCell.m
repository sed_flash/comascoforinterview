//
//  ButtonCell.m
//  stockApp
//
//  Created by Данияр Габбасов on 10.06.16.
//  Copyright © 2016 northalliance. All rights reserved.
//

#import "ButtonCell.h"

NSString * const XLFormRowDescriptorTypeButtonCell = @"XLFormRowDescriptorTypeButtonCell";

@interface ButtonCell()

@property (nonatomic, retain) UIButton *cellButton;

@end

@implementation ButtonCell

+ (void)load
{
    [XLFormViewController.cellClassesForRowDescriptorTypes setObject:[ButtonCell class] forKey:XLFormRowDescriptorTypeButtonCell];
}

- (void)configure {
    [super configure];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)update {
    [super update];
    
    self.cellButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cellButton setBackgroundImage:[UIImage imageNamed:@"button-bg"] forState:UIControlStateNormal];
    [self.cellButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.cellButton addTarget:self action:@selector(cellButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.cellButton setFrame:self.frame];
    [self.cellButton setTitle:self.rowDescriptor.title forState:UIControlStateNormal];
    
    [self.contentView addSubview:self.cellButton];
}

+ (CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor {
    return 44;
}

#pragma mark - Actions

- (IBAction)cellButtonAction:(id)sender {
    if (self.rowDescriptor.action.formSelector){
        [self.formViewController performFormSelector:self.rowDescriptor.action.formSelector withObject:self.rowDescriptor];
    }
}

@end
