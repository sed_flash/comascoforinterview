//
//  ComascoTests.m
//  ComascoTests
//
//  Created by Vlad on 23.06.16.
//  Copyright © 2016 com.app.comasco. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface ComascoTests : XCTestCase

@end

@implementation ComascoTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testWebView {
    UIWebView *testView = [[UIWebView alloc] init];
    testView = nil;
    XCTAssertNil(testView, @"%@ is not nil!", testView);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
